from pathlib import Path
from data_methods_load.methods import download_preprocess
import os

# Setup 1

data_path = Path(__file__) / ".." / Path("data_methods_load/data")

if not os.path.exists(Path(data_path, "US_Accidents_Dec21_updated.csv")):
    print("Downloading US_Accidents dataset")
    try:
        import kaggle
    except OSError as e:
        print(
            "If kaggle.json is not found, please follow instructions here (https://www.kaggle.com/docs/api#getting-started-installation-&-authentication) to create a Kaggle accout and obtain a token."
        )
        raise e

    kaggle.api.dataset_download_files("sobhanmoosavi/us-accidents", path=data_path, unzip=True)

download_preprocess(
    file="US_Accidents_Dec21_updated.csv",
    prep_file="preprocess_us_accidents.ipynb",
    data_path=data_path,
)

download_preprocess(
    file="https://www.umweltbundesamt.de/sites/default/files/medien/6232/dokumente/pharms-uba_v3_2021_0.xlsx",
    prep_file="preprocess_arzneimittel.ipynb",
    data_path=data_path,
)
