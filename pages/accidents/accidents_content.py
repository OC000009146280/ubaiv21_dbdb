import dash
from dash import html, dcc, dash_table
from data_methods_load.datasets import us_accidents
import data_methods_load.datasets_auxiliary as dsa
import data_methods_load.methods as dbm
import plotly.express as px
import dash_bootstrap_components as dbc
import dash_daq as daq


##########################################################################################
# DASHBOARD DEFINITION
##########################################################################################

# dash.register_page(__name__)

layout = dbc.Container(
    className="container-subpages-content-qual",
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["content_all"]["explanation_time_geo"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            id="time-selection-accidents-spinner",
                                            children=[
                                                dcc.Graph(
                                                    id="time-selection-accidents",
                                                    figure=dbm.get_time_barplot(
                                                        dsa.us_accidents_time_counts.index,
                                                        dsa.us_accidents_time_counts.values,
                                                    ),
                                                )
                                            ],
                                            color="primary",
                                        )
                                    ],
                                    width=6,
                                ),
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            id="map-accidents-spinner",
                                            children=[
                                                dcc.Graph(
                                                    id="map_accidents",
                                                )
                                            ],
                                            color="success",
                                        ),
                                        daq.BooleanSwitch(
                                            id="count-switch-accidents",
                                            on=False,
                                            label="Unfälle relativ zur Bevölkerung",
                                            labelPosition="top",
                                        ),
                                    ],
                                    width=6,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["explanation_hist"]["text"],
                                            className="markdown-description",
                                        ),
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["explanation_hist_add_trace"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=12,
                                )
                            ]
                        ),
                        html.Br(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            [dcc.Graph(id="plot-distribution-acc")],
                                            id="plot-distribution-acc-spinner",
                                            color="warning",
                                        )
                                    ]
                                ),
                                dbc.Col(
                                    [
                                        dcc.Dropdown(
                                            id="select-column-distribution-accidents",
                                            options=dbm.get_config_list(
                                                dsa.config_accidents, "display_histogram", "True"
                                            ),
                                            # subset with three to display the fourth item in list as per default
                                            value=dbm.get_config_list(
                                                dsa.config_accidents, "display_histogram", "True"
                                            )[
                                                3
                                            ],  # TODO: Why [3]?
                                        ),
                                        # show distribution
                                        dcc.Dropdown(
                                            id="select-trace-distribution",
                                            placeholder="keine Verteilung anzeigen",
                                            options=[
                                                {"label": "keine Verteilung anzeigen", "value": ""},
                                                {"label": "Normalverteilung anzeigen", "value": "Normal"},
                                                {"label": "Exponentialverteilung anzeigen", "value": "Exponential"},
                                            ],
                                        ),
                                        # generates sliders
                                        html.Div(id="container-show-slider"),
                                        # container that shows current selection of parameters
                                        html.Div(id="output-sliders"),
                                    ]
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["begin_halfsentece_restriction"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=4,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            id="selected-data-percentage-accidents",
                                        ),
                                    ],
                                    width=1,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["halfsentence_restriction_end"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=7,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["content_all"]["explanation_table"]["text"],
                            className="markdown-description",
                        ),
                        dash_table.DataTable(
                            id="acc-tab",
                            # aufgetretener Bug: columns müssen definiert sein, damit filter_action aktiviert werden kann
                            columns=[{"name": col, "id": col} for col in us_accidents.columns],
                            tooltip_header=dbm.get_config_property(dsa.config_accidents, "tooltip"),
                            fixed_rows={"headers": True},
                            style_table={"height": 550, "overflowX": "scroll"},
                            style_header={"backgroundColor": "#305D91", "padding": "10px", "color": "#FFFFFF"},
                            style_cell={
                                "textAlign": "center",
                                "minWidth": 95,
                                "maxWidth": 120,
                                "width": 100,
                                "font_size": "12px",
                                "whiteSpace": "normal",
                                "height": "auto",
                            },
                            # page_action="native",
                            # filter_query='',
                            # filter_action="native",
                            sort_action="native",
                            sort_mode="multi",
                        ),
                    ]
                ),
            ]
        )
    ],
)
