from math import ceil
import dash
from dash import callback, html, dcc, callback, Input, Output, State, ALL
from data_methods_load.datasets import us_accidents
import data_methods_load.methods as dbm
import plotly.express as px
import data_methods_load.datasets_auxiliary as dsa
import dash_bootstrap_components as dbc
import time
from pages.accidents import accidents_content
from pages.accidents import accidents_qual

#####################################################################################################
#####################################################################################################

dash.register_page(__name__)
layout = dbc.Container(
    [
        html.Div(
            className="container-subpages-content-qual",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.Breadcrumb(
                                id="accidents_home_breadcrumb",
                                items=[
                                    {"label": "Home", "href": "/", "external_link": True},
                                    {"label": "Accidents", "active": True},
                                ],
                            )
                        ),
                        dbc.Col(
                            html.Img(
                                src=dash.get_asset_url("UBA_Logo.png"),
                                height=60,
                                width=120,
                                style={"float": "right"},
                            )
                        ),
                    ]
                ),
                html.H2(
                    children="Unfälle in den USA",
                    className="Header",
                ),
                html.P(
                    dsa.markdown["content-qual-metr"],
                    className="markdown-description",
                ),
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            dcc.Graph(
                                id="accidents-radio-graph",
                                figure=dbm.data_quality_radar(dsa.us_accidents_data_quality_scores),
                            ),
                        ),
                        dbc.Col(
                            html.Div(
                                [
                                    html.H4("Qualitätsmetriken", className="subheader"),
                                    dbc.Button(
                                        f'Informationsdichte: {round(dsa.us_accidents_data_quality_scores["Informationsdichte"], 3)}',
                                        color="light",
                                        id="button-toggle-informationsdichte",
                                    ),
                                    dbc.Toast(
                                        header="Informationsdichte",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Informationsdichte"],
                                                    mathjax=True,
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="infobox-informationsdichte",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f'Vollständigkeit: {round(dsa.us_accidents_data_quality_scores["Vollständigkeit"], 3)}',
                                        color="light",
                                        id="button-toggle-vollständigkeit",
                                    ),
                                    dbc.Toast(
                                        header="Vollständigkeit",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Vollständigkeit"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="infobox-vollständigkeit",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f'Singularität: {round(dsa.us_accidents_data_quality_scores["Singularität"], 3)}',
                                        color="light",
                                        id="button-toggle-uniqueness",
                                    ),
                                    dbc.Toast(
                                        header="Singularität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Singularität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="infobox-uniqueness",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f'Validität: {round(dsa.us_accidents_data_quality_scores["Validität"], 3)}',
                                        color="light",
                                        id="button-toggle-Validität",
                                    ),
                                    dbc.Toast(
                                        header="Validität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Validität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="infobox-Validität",
                                        is_open=False,
                                    ),
                                ]
                            )
                        ),
                    ]
                ),
                dcc.Markdown(
                    dsa.markdown["content_all"]["top-of-tabs"]["text"],
                    className="markdown-description",
                ),
                dcc.Tabs(
                    id="tabs-accidents",
                    value="tab-accidents-content",
                    children=[
                        dcc.Tab(
                            className="tab-subpages",
                            label="Inhalt",
                            value="tab-accidents-content",
                            id="tab-accidents-content",
                            selected_style={"font-weight": "bold"},
                        ),
                        dcc.Tab(
                            className="tab-subpages",
                            label="Datenqualität",
                            value="tab-accidents-quality",
                            id="tab-accidents-quality",
                            selected_style={"font-weight": "bold"},
                        ),
                    ],
                ),
                html.Div(id="tabs-content-acc"),
                html.Br(),
                html.Footer(
                    [
                        html.P(
                            [
                                "Datensatz: ",
                                html.A(
                                    "US Accidents",
                                    href="https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents",
                                    target="_blank",
                                ),
                            ],
                        ),
                        html.P(
                            [
                                "Bereitgestellt durch: ",
                                "Kaggle"
                                #                            html.A(
                                #                                "Kaggle", href="https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents", target="_blank"
                                #                            )
                            ],
                        ),
                    ]
                ),
            ],
        )
    ]
)

#####################################################################################################
#####################################################################################################


@callback(Output("tabs-content-acc", "children"), Input("tabs-accidents", "value"))
def render_content(tab):
    if tab == "tab-accidents-content":
        return accidents_content.layout
    elif tab == "tab-accidents-quality":
        return accidents_qual.layout


# @callback(
#     Output("accidents-radio-graph", "figure"),
#     Output("button-toggle-informationsdichte", "children"),
#     Output("button-toggle-vollständigkeit", "children"),
#     Output("button-toggle-uniqueness", "children"),
#     Output("button-toggle-Validität", "children"),
#     Input("select-column-distribution-accidents", "value"),
#     Input("plot-distribution-acc", "selectedData"),
#     Input("time-selection-accidents", "selectedData"),
#     Input("map_accidents", "selectedData"),
# )
# def update_quality_radar(filter_dim, filter_range, selected_time_data, selected_map_data):
#     tx = time.time()
#     data = dbm._filter_distribution_dimension(filter_dim=filter_dim, selected_data=filter_range, data=us_accidents)
#     data = dbm._filter_time_selection(selected_time_data, data, time_col="Year")
#     inverted_mapping = {v: k for k, v in dsa.us_accidents_mapping["State"].items()}
#     if selected_map_data is not None:
#         for i, p in enumerate(selected_map_data["points"]):
#             selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
#     data = dbm._filter_country_select(selected_map_data, data, location_mapping_col="State")
#     msn = dbm.data_conformity_checks(data, dsa.config_accidents)
#     corr = data.corr()
#     p = dbm.calculate_pvalues(data)
#     data_quality_scores = dbm.data_quality_scores(
#         correlations=corr,
#         p_values=p,
#         missing_values_df=msn,
#         dataset=data,
#     )
#     fig = dbm.data_quality_radar(data_quality_scores)
#     button1 = f'Informationsdichte: {round(data_quality_scores["Informationsdichte"], 3)}'
#     button2 = f'Vollständigkeit: {round(data_quality_scores["Vollständigkeit"], 3)}'
#     button4 = f'Singularität: {round(data_quality_scores["Singularität"], 3)}'
#     button5 = f'Validität: {round(data_quality_scores["Validität"], 3)}'
#     print(f"done executing the update_quality_radar_callback after {(time.time()-tx)*1e3:.2f} ms")
#     return fig, button1, button2, button4, button5


@callback(
    Output("infobox-informationsdichte", "is_open"),
    Input("button-toggle-informationsdichte", "n_clicks"),
    State("infobox-informationsdichte", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("infobox-vollständigkeit", "is_open"),
    Input("button-toggle-vollständigkeit", "n_clicks"),
    State("infobox-vollständigkeit", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("infobox-uniqueness", "is_open"),
    Input("button-toggle-uniqueness", "n_clicks"),
    State("infobox-uniqueness", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("infobox-Validität", "is_open"),
    Input("button-toggle-Validität", "n_clicks"),
    State("infobox-Validität", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


####################################################################################################
# CONTENT CALLBACKS
####################################################################################################
@callback(Output("count-switch-accidents", "label"), Input("count-switch-accidents", "on"))
def update_switch_label(switch):
    """
    used to switch between two button titles for map based on accidents per capita or on the total number of accidents
        Input:
            switch: boolean
        Output:
            str: Switch Label
    """
    if switch:
        return "Unfälle relativ zur Bevölkerung des Bundesstaats"
    else:
        return "Absolute Anzahl an Unfällen pro Bundesstaat"


@callback(Output("map_accidents", "figure"), Input("count-switch-accidents", "on"))
# Nicht sinnvoll, usa_map aggregiert unfälle von 2016 - 2021
def update_map(switch):
    """
    uses display_map from the methods file to either display a map based on accidents per capita or on the total number of accidents
        Input:
            switch: boolean
        Output:
            fig: choropleth figure
    """
    if switch:
        fig = dbm.display_Map(
            data_frame=dbm.remap(dsa.us_accidents_map_df, "State", dsa.us_accidents_mapping["State"]),
            color="per_capita",
            locations="State",
            locationmode="USA-states",
            featureidkey="properties.Factor",
            scope="usa",
            labels={"per_capita": "Unfälle pro Kopf"},
        )
    else:
        fig = dbm.display_Map(
            data_frame=dbm.remap(dsa.us_accidents_map_df, "State", dsa.us_accidents_mapping["State"]),
            color=0,
            locations="State",
            locationmode="USA-states",
            featureidkey="properties.Factor",
            scope="usa",
            labels={"0": "Summe"},
        )
    return fig


@callback(
    Output("acc-tab", "data"),
    Input("select-column-distribution-accidents", "value"),
    Input("plot-distribution-acc", "selectedData"),
    Input("time-selection-accidents", "selectedData"),
    Input("map_accidents", "selectedData"),
)
def data_table_selection(filter_dim, filter_range, selected_time_data, selected_map_data):
    """
    used to filter the dataset based on geographical, temporal or value-related restrictions
        Input:
            filter_dim: column chosen for distribution plot
            filter_range: filters applied to distribution plot
            selected_time_data: filters applied to histogram
            selected_map_dat: filters applied to map
        Output:
            dictionary
    """
    data = dbm._filter_distribution_dimension(filter_dim=filter_dim, selected_data=filter_range, data=us_accidents)
    data = dbm._filter_time_selection(selected_time_data, data, time_col="Year")
    inverted_mapping = {v: k for k, v in dsa.us_accidents_mapping["State"].items()}
    if selected_map_data is not None:
        for i, p in enumerate(selected_map_data["points"]):
            selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
    data = dbm._filter_country_select(selected_map_data, data, location_mapping_col="State")
    if len(data) > 10:
        remapped_sample = data.sample(10)
    else:
        remapped_sample = data.sample(len(data))
    for col in ["Side", "City", "County", "State", "Timezone", "Airport_Code"]:
        remapped_sample = dbm.remap(remapped_sample, col, dsa.us_accidents_mapping[col])
    return remapped_sample.to_dict("records")


@callback(
    Output("selected-data-percentage-accidents", "children"),
    Output("selected-data-percentage-accidents", "style"),
    Input("select-column-distribution-accidents", "value"),
    Input("plot-distribution-acc", "selectedData"),
    Input("time-selection-accidents", "selectedData"),
    Input("map_accidents", "selectedData"),
)
def selected_data_percentage(plot_dim, plot_selected_range, selected_time_data, selected_map_data):
    data = dbm._filter_distribution_dimension(filter_dim=plot_dim, selected_data=plot_selected_range, data=us_accidents)
    data = dbm._filter_time_selection(selected_time_data, data, time_col="Year")
    inverted_mapping = {v: k for k, v in dsa.us_accidents_mapping["State"].items()}
    if selected_map_data is not None:
        for i, p in enumerate(selected_map_data["points"]):
            selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
    data = dbm._filter_country_select(selected_map_data, data, location_mapping_col="State")

    fraction = len(data.index) / len(us_accidents.index)

    color_steps = 10
    color = px.colors.sample_colorscale("Rainbow", color_steps, 0.55)[-ceil(min(fraction, 0.99) * color_steps)]
    return "{frac:.0%}".format(frac=fraction), {"color": color}


@callback(
    Output("plot-distribution-acc", "figure"),
    Input("select-column-distribution-accidents", "value"),
    Input("select-trace-distribution", "value"),
    Input({"type": "distribution_parameter_slider", "index": ALL}, "value"),
)
def display_distribution(selected_column, distribution, slider_values):
    display_list = dbm.get_config_list(dsa.config_accidents, "display_histogram", "True")
    if selected_column in dsa.us_accidents_mapping.keys():
        df = dbm.remap(us_accidents[display_list], selected_column, dsa.us_accidents_mapping[selected_column])
    else:
        df = us_accidents[display_list]
    fig = dbm.display_histogram(data_frame=df, x=selected_column)

    if distribution and slider_values:
        df_subs = us_accidents[selected_column]
        trace = dbm.make_trace(df_subs, distribution, slider_values)
        fig = fig.add_trace(trace)
        print(f"{distribution} trace added")

    return fig


@callback(
    Output("container-show-slider", "children"),
    Input("select-column-distribution-accidents", "value"),
    Input("select-trace-distribution", "value"),
)
def display_sliders(df_col, distr):
    slider_list = []
    numeric_features_list = dbm.get_config_list(dsa.config_accidents, "numeric_feature_slider", "True")
    if df_col in numeric_features_list:
        if distr:
            df_subset = us_accidents[df_col]
            stepsize_dict = {"Normal": 1, "Exponential": 0.1}

            outputs = dbm.get_ranges_dist(df_subset, distr)

            slider_list = [
                html.Div(
                    [
                        parameter,
                        dcc.Slider(
                            min=p_min,
                            max=p_max,
                            step=stepsize_dict[distr],
                            value=p_init,
                            marks=None,
                            tooltip={"placement": "bottom", "always_visible": True},
                            id={"type": "distribution_parameter_slider", "index": parameter},
                        ),
                    ]
                )
                for p_init, p_min, p_max, parameter in zip(*outputs)
            ]

    return slider_list


####################################################################################################
# QUALITY CALLBACKS
####################################################################################################


@callback(Output("corr-plot", "figure"), Input("p-switch-accidents", "on"))
def display_corrmap(switch):
    if switch:
        fig = dbm.display_corr_map(dsa.us_accidents_p, px.colors.sequential.Greys, "p")

    else:
        fig = dbm.display_corr_map(dsa.us_accidents_corr, px.colors.diverging.delta, "corr")
    return fig


@callback(
    Output("numerical-column-test-output", "children"),
    Input({"type": "quality-check-rb", "col": ALL, "row": "Numerical"}, "on"),
)
def quality_check_numerical(button_state):
    """
    Move to methods once it works to reuse on other quality pages
    """
    try:
        column = dash.ctx.triggered_id["col"]
    except TypeError:
        column = "No"
    return f"{column} numerical check switched"


@callback(
    Output("natural-column-test-output", "children"),
    Input({"type": "quality-check-rb", "col": ALL, "row": "Natural"}, "on"),
)
def quality_check_natural(button_state):
    """
    Move to methods once it works to reuse on other quality pages
    """
    try:
        column = dash.ctx.triggered_id["col"]
    except TypeError:
        column = "No"
    return f"{column} natural check switched"
