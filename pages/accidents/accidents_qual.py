from email import charset
from dash import html, dcc
import data_methods_load.datasets_auxiliary as dsa
import data_methods_load.methods as dbm
import dash_daq as daq
import dash_bootstrap_components as dbc


##########################################################################################
# DASHBOARD DEFINITION
##########################################################################################


# dash.register_page(__name__)
layout = dbc.Container(
    className="container-subpages-content-qual",
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["qual_all"]["explanation_columnbars"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dcc.Graph(id="msn-plot", figure=dbm.display_missing_barplot(dsa.us_accidents_msn)),  # type: ignore
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["explanation_correlationheatmap"]["text"],
                            className="markdown-description",
                        ),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["correlation_formula"]["text"],
                            mathjax=True,
                            className="markdown-description",
                            style={"text-align": "center"},
                        ),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["explanation_pvalues"]["text"],
                            className="markdown-description",
                        ),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Graph(id="corr-plot"),  # type: ignore
                                    ],
                                    width=8,  # type: ignore
                                ),
                                dbc.Col(
                                    [
                                        daq.BooleanSwitch(  # type: ignore
                                            id="p-switch-accidents", on=False, label="p-Werte", labelPosition="top"
                                        ),
                                    ],
                                    width=4,  # type: ignore
                                ),
                            ]
                        ),
                    ]
                ),
            ]
        )
    ],
)
