from dash import html, dcc
import data_methods_load.datasets_auxiliary as dsa
from data_methods_load.methods import display_missing_barplot
import dash_daq as daq
import dash_bootstrap_components as dbc


#############################################################################################################
# LAYOUT
#############################################################################################################
layout = dbc.Container(
    className="container-subpages-content-qual",
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["qual_all"]["explanation_columnbars"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dcc.Graph(id="msn-plot-pa", figure=display_missing_barplot(dsa.arzneimittel_pa_msn)),
                            ]
                        ),
                    ]
                ),
            ]
        )
    ],
)
