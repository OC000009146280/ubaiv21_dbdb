from dash import html, dcc
import data_methods_load.datasets_auxiliary as dsa
from data_methods_load.methods import display_missing_barplot
import dash_daq as daq
import dash_bootstrap_components as dbc
import data_methods_load.methods as dbm


#############################################################################################################
# LAYOUT
#############################################################################################################
layout = dbc.Container(
    className="container-subpages-content-qual",
    children=[
        html.Div(
            children=[
                dcc.Markdown(
                    dsa.markdown["qual_all"]["explanation_columnbars"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dcc.Graph(id="msn-plot-mec", figure=display_missing_barplot(dsa.arzneimittel_mec_msn)),
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["explanation_correlationheatmap"]["text"],
                            className="markdown-description",
                        ),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["correlation_formula"]["text"],
                            mathjax=True,
                            className="markdown-description",
                            style={"text-align": "center"},
                        ),
                        dcc.Markdown(
                            dsa.markdown["qual_all"]["explanation_pvalues"]["text"],
                            className="markdown-description",
                        ),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Graph(id="corr-plot-mec"),
                                    ],
                                    width=8,
                                ),
                                dbc.Col(
                                    [
                                        daq.BooleanSwitch(
                                            id="p-switch-mec", on=False, label="p-values", labelPosition="top"
                                        ),
                                    ],
                                    width=4,
                                ),
                            ]
                        ),
                    ]
                ),
            ]
        )
    ],
)
