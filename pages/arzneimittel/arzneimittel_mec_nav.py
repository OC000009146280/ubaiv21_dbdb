from math import ceil
import dash
from dash import callback, html, dcc, Input, Output, State
import dash_bootstrap_components as dbc
import plotly.express as px
import data_methods_load.methods as dbm
from data_methods_load.datasets import arzneimittel_mec as mec
import data_methods_load.datasets_auxiliary as dsa
from pages.arzneimittel import arzneimittel_mec
from pages.arzneimittel import arzneimittel_mec_qual


#######################################################################################
#######################################################################################

dash.register_page(__name__)
layout = dbc.Container(
    [
        html.Div(
            className="container-subpages-content-qual",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.Breadcrumb(
                                id="mec-home-breadcrumb",
                                items=[
                                    {"label": "Home", "href": "/", "external_link": True},
                                    {
                                        "label": "Arzneimittel",
                                        "href": "/arzneimittel/arzneimittel-nav",
                                        "external_link": True,
                                    },
                                    {"label": "Arzneimittel Messungen", "active": True},
                                ],
                            ),
                        ),
                        dbc.Col(
                            html.Img(
                                src=dash.get_asset_url("UBA_Logo.png"),
                                height=60,
                                width=120,
                                style={"float": "right"},
                            )
                        ),
                    ]
                ),
                html.H2(
                    children="Arzneimittel - Messungen",
                    className="Header",
                ),
                dbc.Row(
                    [
                        dbc.Col(
                            dcc.Graph(
                                id="mec-radio-graph",
                                figure=dbm.data_quality_radar(dsa.arzneimittel_mec_data_quality_scores),
                            ),
                        ),
                        dbc.Col(
                            html.Div(
                                [
                                    html.H4("Qualitätsmetriken", className="subheader"),
                                    dbc.Button(
                                        f"Informationsdichte: {round(dsa.arzneimittel_mec_data_quality_scores['Informationsdichte'], 3)}",
                                        color="light",
                                        id="mec-button-toggle-informationsdichte",
                                    ),
                                    dbc.Toast(
                                        header="Informationsdichte",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Informationsdichte"],
                                                    mathjax=True,
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="mec-infobox-informationsdichte",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Vollständigkeit: {round(dsa.arzneimittel_mec_data_quality_scores['Vollständigkeit'], 3)}",
                                        color="light",
                                        id="mec-button-toggle-vollständigkeit",
                                    ),
                                    dbc.Toast(
                                        header="Vollständigkeit",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Vollständigkeit"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="mec-infobox-vollständigkeit",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Singularität: {round(dsa.arzneimittel_mec_data_quality_scores['Singularität'], 3)}",
                                        color="light",
                                        id="mec-button-toggle-uniqueness",
                                    ),
                                    dbc.Toast(
                                        header="Singularität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Singularität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="mec-infobox-uniqueness",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Validität: {round(dsa.arzneimittel_mec_data_quality_scores['Validität'], 3)}",
                                        color="light",
                                        id="mec-button-toggle-Validität",
                                    ),
                                    dbc.Toast(
                                        header="Validität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Validität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="mec-infobox-Validität",
                                        is_open=False,
                                    ),
                                ]
                            )
                        ),
                    ]
                ),
                dcc.Markdown(
                    dsa.markdown["content_all"]["top-of-tabs"]["text"],
                    className="markdown-description",
                ),
                html.Br(),
                dcc.Tabs(
                    id="tabs-arzneimittel-mec",
                    value="tab-arzneimittel-mec-content",
                    children=[
                        dcc.Tab(
                            className="tab-subpages",
                            id="tabs-mec-content",
                            label="Inhalt",
                            value="tab-arzneimittel-mec-content",
                            selected_style={"font-weight": "bold"},
                        ),
                        dcc.Tab(
                            className="tab-subpages",
                            id="tabs-mec-quality",
                            label="Qualität",
                            value="tab-arzneimittel-mec-quality",
                            selected_style={"font-weight": "bold"},
                        ),
                    ],
                ),
                html.Div(id="tabs-content-arzneimittel-mec"),
                html.Br(),
                html.Footer(
                    [
                        html.P(
                            [
                                "Datensatz: ",
                                html.A(
                                    "Arzneimittel in der Umwelt",
                                    href="https://www.umweltbundesamt.de/themen/chemikalien/arzneimittel/humanarzneimittel/arzneimittel-umwelt",
                                    target="_blank",
                                ),
                            ],
                        ),
                        html.P(
                            [
                                "Bereitgestellt durch: ",
                                "Umweltbundesamt"
                                #                            html.A(
                                #                                "Kaggle", href="https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents", target="_blank"
                                #                            )
                            ],
                        ),
                    ]
                ),
            ],
        )
    ]
)

#######################################################################################
# NAVIGATOR CALLBACKS
#######################################################################################
@callback(Output("tabs-content-arzneimittel-mec", "children"), [Input("tabs-arzneimittel-mec", "value")])
def render_content(tab):
    if tab == "tab-arzneimittel-mec-content":
        return arzneimittel_mec.layout
    elif tab == "tab-arzneimittel-mec-quality":
        return arzneimittel_mec_qual.layout


# @callback(
#     Output("mec-radio-graph", "figure"),
#     Output("mec-button-toggle-informationsdichte", "children"),
#     Output("mec-button-toggle-vollständigkeit", "children"),
#     Output("mec-button-toggle-uniqueness", "children"),
#     Output("mec-button-toggle-Validität", "children"),
#     Input("select-column-distribution-mec", "value"),
#     Input("plot-distribution-mec", "selectedData"),
#     Input("time-selection-arzneimittel-mec", "selectedData"),
#     Input("map_arzneimittel", "selectedData"),
# )
# def update_quality_radar(filter_dim, filter_vals, selected_time_data, selected_map_data):
#     data = dbm._filter_distribution_dimension(filter_dim, filter_vals, data=mec)
#     data = dbm._filter_time_selection(selected_time_data, data, time_col="Sampling Period Start")
#     inverted_mapping = {v: k for k, v in dsa.arzneimittel_mec_mapping["Sampling Country"].items()}
#     if selected_map_data is not None:
#         for i, p in enumerate(selected_map_data["points"]):
#             selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
#     data = dbm._filter_country_select(selected_map_data, data, "Sampling Country")
#     msn = dbm.data_conformity_checks(data, dsa.config_mec)
#     corr = data.corr()
#     p = dbm.calculate_pvalues(data)
#     data_quality_scores = dbm.data_quality_scores(
#         correlations=corr,
#         p_values=p,
#         missing_values_df=msn,
#         dataset=data,
#     )
#     fig = dbm.data_quality_radar(data_quality_scores)
#     button1 = f"Informationsdichte: {round(data_quality_scores['Informationsdichte'], 3)}"
#     button2 = f"Vollständigkeit: {round(data_quality_scores['Vollständigkeit'], 3)}"
#     button4 = f"Singularität: {round(data_quality_scores['Singularität'], 3)}"
#     button5 = f"Validität: {round(data_quality_scores['Validität'], 3)}"
#     return fig, button1, button2, button4, button5


@callback(
    Output("mec-infobox-informationsdichte", "is_open"),
    Input("mec-button-toggle-informationsdichte", "n_clicks"),
    State("mec-infobox-informationsdichte", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("mec-infobox-vollständigkeit", "is_open"),
    Input("mec-button-toggle-vollständigkeit", "n_clicks"),
    State("mec-infobox-vollständigkeit", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("mec-infobox-uniqueness", "is_open"),
    Input("mec-button-toggle-uniqueness", "n_clicks"),
    State("mec-infobox-uniqueness", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("mec-infobox-Validität", "is_open"),
    Input("mec-button-toggle-Validität", "n_clicks"),
    State("mec-infobox-Validität", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


#######################################################################################
# MEC CONTENT CALLBACKS
#######################################################################################


@callback(
    Output("mec-data-table", "data"),
    Input("select-column-distribution-mec", "value"),
    Input("plot-distribution-mec", "selectedData"),
    Input("time-selection-arzneimittel-mec", "selectedData"),
    Input("map_arzneimittel", "selectedData"),
)
def data_table_selection(filter_dim, filter_vals, selected_time_data, selected_map_data):  #
    """
    used to filter the dataset based on geographical, temporal or value-related restrictions
        Input:
            filter_dim: column chosen for distribution plot
            filter_range: filters applied to distribution plot
            selected_time_data: filters applied to histogram
            selected_map_dat: filters applied to map
        Output:
            dictionary
    """

    data = dbm._filter_distribution_dimension(filter_dim, filter_vals, data=mec)
    data = dbm._filter_time_selection(selected_time_data, data, time_col="Sampling Period Start")
    inverted_mapping = {v: k for k, v in dsa.arzneimittel_mec_mapping["Sampling Country"].items()}
    if selected_map_data is not None:
        for i, p in enumerate(selected_map_data["points"]):
            selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
    data = dbm._filter_country_select(selected_map_data, data, "Sampling Country")
    if len(data) > 10:
        remapped_sample = data.sample(10)
    else:
        remapped_sample = data.sample(len(data))
    for col in dsa.arzneimittel_mec_mapping.keys():
        remapped_sample = dbm.remap(remapped_sample, col, dsa.arzneimittel_mec_mapping[col])
    return remapped_sample.to_dict("records")


@callback(
    Output("selected-data-percentage-arzneimittel-mec", "children"),
    Output("selected-data-percentage-arzneimittel-mec", "style"),
    Input("select-column-distribution-mec", "value"),
    Input("plot-distribution-mec", "selectedData"),
    Input("time-selection-arzneimittel-mec", "selectedData"),
    Input("map_arzneimittel", "selectedData"),
)
def selected_data_percentage(filter_dim, filter_vals, selected_time_data, selected_map_data):
    """
    calculates percentage of selected data

        Input:
            filter_dim: column to filter
            filter_vals: filters applied to column
            selected_time_data: filters applied to histogram
            selected_map_dat: filters applied to map

        Returns:
            string: information about the percentage of data selected
    """

    data = dbm._filter_distribution_dimension(filter_dim, filter_vals, data=mec)
    data = dbm._filter_time_selection(selected_time_data, data=data, time_col="Sampling Period Start")
    inverted_mapping = {v: k for k, v in dsa.arzneimittel_mec_mapping["Sampling Country"].items()}
    if selected_map_data is not None:
        for i, p in enumerate(selected_map_data["points"]):
            selected_map_data["points"][i]["location"] = int(inverted_mapping[p["location"]])
    data = dbm._filter_country_select(selected_map_data, data=data, location_mapping_col="Sampling Country")

    fraction = len(data.index) / len(mec.index)

    color_steps = 10
    # color = px.colors.sample_colorscale("Bluered", color_steps)[-int(min(fraction, 0.99) * color_steps)]
    color = px.colors.sample_colorscale("Rainbow", color_steps, 0.55)[-ceil(min(fraction, 0.99) * color_steps)]
    return "{frac:.0%}".format(frac=fraction), {"color": color}


@callback(
    Output("plot-distribution-mec", "figure"),
    Input("select-column-distribution-mec", "value"),
)
def display_distribution(val):
    """
    displays distribution plot for selected column

        Input:
            selected_column: column selected in Dropdown
            distribution: chosen distribution
            slider_values: values of the sliders

        Returns:
            fig: figure of the distribution plot
    """

    fig = dbm.display_histogram(data_frame=mec[arzneimittel_mec.arzneimittel_mec_display_list], x=val)
    return fig


#######################################################################################
# MEC QUALITY CALLBACKS
#######################################################################################


@callback(Output("corr-plot-mec", "figure"), Input("p-switch-mec", "on"))
def display_corrmap(switch):
    """
    displays either correlation heatmap or p-value-heatmap

        Input:
            switch: boolean

        Returns:
            fig: figure of the distribution plot
    """

    if switch:
        fig = dbm.display_corr_map(dsa.arzneimittel_mec_p, px.colors.sequential.Greys, "p")

    else:
        fig = dbm.display_corr_map(dsa.arzneimittel_mec_corr, px.colors.diverging.delta, "corr")
    return fig
