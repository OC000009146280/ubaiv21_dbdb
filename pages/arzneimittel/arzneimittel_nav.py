import dash
from dash import html
import dash_bootstrap_components as dbc


dash.register_page(__name__)


layout = html.Div(
    className="container-subpages-content-qual",
    children=[
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Breadcrumb(
                        items=[
                            {"label": "Home", "href": "/", "external_link": True},
                            {"label": "Arzneimittel", "active": True},
                        ]
                    ),
                ),
                dbc.Col(
                    html.Img(
                        src=dash.get_asset_url("UBA_Logo.png"),
                        height=60,
                        width=120,
                        style={"float": "right"},
                    )
                ),
            ]
        ),
        html.H2(
            "Arzneimittel in der Umwelt",
            className="Header",
            # style={
            #     "text-decoration": "underline",
            #     "margin-left": "100px",
            #     "color": "black",
            #     "border": "4px solid black",
            #     "border-radius": "12px",
            #     "padding": "10px",
            #     "text-align": "center",
            #     "width": "600px",
            # },
        ),
        html.Br(),
        dbc.Row(
            [
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            [
                                dbc.Card(
                                    [
                                        dbc.CardImg(
                                            src=dash.get_asset_url("pexels-arzneimittel-messungen.png"),
                                            top=True,
                                            id="CardImg_arzneimittel_mec",
                                        ),
                                        html.H4("Messungen", className="card-title"),
                                        html.P(
                                            " Ein Datensatz zu verschiedenen Studien bei denen "
                                            " Arzneimittel in der Umwelt festgestellt wurden. ",
                                            className="card-text",
                                        ),
                                        dbc.Button(
                                            "Overview",
                                            color="primary",
                                            outline=True,
                                            href="/arzneimittel/arzneimittel-mec-nav",
                                            id="Button_arzneimittel_mec",
                                        ),
                                    ],
                                    style={"width": "18rem", "padding": "5px"},
                                    className="small-card",
                                    id="arzneimittel_mec_card",
                                ),
                            ],
                            width={"size": 4, "offset": 1},
                        ),
                        dbc.Col(
                            [
                                dbc.Card(
                                    [
                                        dbc.CardImg(
                                            src=dash.get_asset_url("pexels-pharmaagents.png"),
                                            top=True,
                                            id="CardImg_arzneimittel_pa",
                                        ),
                                        html.H4("Pharma Agents", className="card-title"),
                                        html.P(
                                            " Ein Datensatz zu verschiedenen Arzneimitteln die bei Messungen "
                                            " in der Umwelt festgestellt wurden. ",
                                            className="card-text",
                                        ),
                                        dbc.Button(
                                            "Overview",
                                            color="primary",
                                            outline=True,
                                            href="/arzneimittel/arzneimittel-pa-nav",
                                            id="Button_arzneimittel_pa",
                                        ),
                                    ],
                                    style={"width": "18rem", "padding": "5px"},
                                    className="small-card",
                                    id="arzneimittel_pa_card",
                                )
                            ],
                            width=4,
                        ),
                    ]
                ),
            ],
        ),
    ],
)
