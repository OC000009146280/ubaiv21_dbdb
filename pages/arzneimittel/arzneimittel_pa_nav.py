from math import ceil
import dash
from dash import callback, html, dcc, Input, Output, State
import dash_bootstrap_components as dbc
import plotly.express as px
import data_methods_load.methods as dbm
from data_methods_load.datasets import arzneimittel_pa as pa
import data_methods_load.datasets_auxiliary as dsa
from pages.arzneimittel import arzneimittel_pa
from pages.arzneimittel import arzneimittel_pa_qual


#######################################################################################
#######################################################################################

dash.register_page(__name__)
layout = dbc.Container(
    [
        html.Div(
            className="container-subpages-content-qual",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.Breadcrumb(
                                items=[
                                    {"label": "Home", "href": "/", "external_link": True},
                                    {
                                        "label": "Arzneimittel",
                                        "href": "/arzneimittel/arzneimittel-nav",
                                        "external_link": True,
                                    },
                                    {"label": "Arzneimittel Pharma Agents", "active": True},
                                ]
                            ),
                        ),
                        dbc.Col(
                            html.Img(
                                src=dash.get_asset_url("UBA_Logo.png"),
                                height=60,
                                width=120,
                                style={"float": "right"},
                            )
                        ),
                    ]
                ),
                html.H2(
                    children="Arzneimittel - Pharma Agents",
                    className="Header",
                ),
                dbc.Row(
                    [
                        dbc.Col(
                            dcc.Graph(
                                id="pa-radio-graph",
                                figure=dbm.data_quality_radar(dsa.arzneimittel_pa_data_quality_scores),
                            ),
                        ),
                        dbc.Col(
                            html.Div(
                                [
                                    html.H4("Qualitätsmetriken", className="subheader"),
                                    dbc.Button(
                                        f"Informationsdichte: {round(dsa.arzneimittel_pa_data_quality_scores['Informationsdichte'], 3)}",
                                        color="light",
                                        id="pa-button-toggle-informationsdichte",
                                    ),
                                    dbc.Toast(
                                        header="Informationsdichte",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Informationsdichte"],
                                                    mathjax=True,
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="pa-infobox-informationsdichte",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Vollständigkeit: {round(dsa.arzneimittel_pa_data_quality_scores['Vollständigkeit'], 3)}",
                                        color="light",
                                        id="pa-button-toggle-vollständigkeit",
                                    ),
                                    dbc.Toast(
                                        header="Vollständigkeit",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Vollständigkeit"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="pa-infobox-vollständigkeit",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Singularität: {round(dsa.arzneimittel_pa_data_quality_scores['Singularität'], 3)}",
                                        color="light",
                                        id="pa-button-toggle-uniqueness",
                                    ),
                                    dbc.Toast(
                                        header="Singularität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Singularität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="pa-infobox-uniqueness",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Validität: {round(dsa.arzneimittel_pa_data_quality_scores['Validität'], 3)}",
                                        color="light",
                                        id="pa-button-toggle-Validität",
                                    ),
                                    dbc.Toast(
                                        header="Validität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Validität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="pa-infobox-Validität",
                                        is_open=False,
                                    ),
                                ]
                            )
                        ),
                    ]
                ),
                dcc.Markdown(
                    dsa.markdown["content_all"]["top-of-tabs"]["text"],
                    className="markdown-description",
                ),
                html.Br(),
                dcc.Tabs(
                    id="tabs-arzneimittel-pa",
                    value="tab-arzneimittel-pa-content",
                    children=[
                        dcc.Tab(
                            label="Inhalt",
                            value="tab-arzneimittel-pa-content",
                            style={"background-color": "#ffffff"},
                            selected_style={"font-weight": "bold"},
                        ),
                        dcc.Tab(
                            label="Qualität",
                            value="tab-arzneimittel-pa-quality",
                            style={"background-color": "#ffffff"},
                            selected_style={"font-weight": "bold"},
                        ),
                    ],
                ),
                html.Div(id="tabs-content-arzneimittel-pa"),
                html.Br(),
                html.Footer(
                    [
                        html.P(
                            [
                                "Datensatz: ",
                                html.A(
                                    "Arzneimittel in der Umwelt",
                                    href="https://www.umweltbundesamt.de/themen/chemikalien/arzneimittel/humanarzneimittel/arzneimittel-umwelt",
                                    target="_blank",
                                ),
                            ],
                        ),
                        html.P(
                            [
                                "Bereitgestellt durch: ",
                                "Umweltbundesamt"
                                #                            html.A(
                                #                                "Kaggle", href="https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents", target="_blank"
                                #                            )
                            ],
                        ),
                    ]
                ),
            ],
        )
    ]
)

#######################################################################################
# NAVIGATOR CALLBACKS
#######################################################################################
@callback(Output("tabs-content-arzneimittel-pa", "children"), [Input("tabs-arzneimittel-pa", "value")])
def render_content(tab):
    if tab == "tab-arzneimittel-pa-content":
        return arzneimittel_pa.layout
    elif tab == "tab-arzneimittel-pa-quality":
        return arzneimittel_pa_qual.layout


@callback(
    Output("pa-infobox-informationsdichte", "is_open"),
    Input("pa-button-toggle-informationsdichte", "n_clicks"),
    State("pa-infobox-informationsdichte", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("pa-infobox-vollständigkeit", "is_open"),
    Input("pa-button-toggle-vollständigkeit", "n_clicks"),
    State("pa-infobox-vollständigkeit", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("pa-infobox-uniqueness", "is_open"),
    Input("pa-button-toggle-uniqueness", "n_clicks"),
    State("pa-infobox-uniqueness", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("pa-infobox-Validität", "is_open"),
    Input("pa-button-toggle-Validität", "n_clicks"),
    State("pa-infobox-Validität", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


#######################################################################################
# PA CONTENT CALLBACKS
#######################################################################################


# @callback(
#     Output("pharma-agents-data-table", "data"),
#     Input("select-pa-distribution", "value"),
#     Input("plot-pa-distribution", "selectedData"),
# )
# def data_table_content(dimension, selection):
#     if dimension and selection:
#         selected_data = pa.loc[pa[dimension].isin(selection)]
#     else:
#         selected_data = pa
#     return selected_data.to_dict("records")


# @callback(Output("dimension-dropdown", "options"), Input("dimension-selection", "value"))
# def dimension_dropdown_options(dimension):
#     if dimension:
#         return pa.value_counts(subset=dimension).index.values
#     else:
#         return []


@callback(
    Output("selected-data-percentage-arzneimittel-pa", "children"),
    Output("selected-data-percentage-arzneimittel-pa", "style"),
    Input("select-pa-distribution", "value"),
    Input("plot-pa-distribution", "selectedData"),
)
def selected_data_percentage(plot_dim, plot_selected_range):
    data = dbm._filter_distribution_dimension(filter_dim=plot_dim, selected_data=plot_selected_range, data=pa)
    fraction = len(data.index) / len(pa.index)
    color_steps = 10
    color = px.colors.sample_colorscale("Rainbow", color_steps, 0.55)[-ceil(min(fraction, 0.99) * color_steps)]
    # return "{:.0%}".format(fraction), {"color": color}
    return "{frac:.0%}".format(frac=fraction), {"color": color}


#    total = len(pa.index)
#    if dimension and selection:
#        part = len(pa.loc[pa[dimension].isin(selection)].index)
#    else:
#        part = total
#    fraction = part / total
#    color_steps = 10
#    color = px.colors.sample_colorscale("Bluered", color_steps)[-int(min(fraction, 0.99) * color_steps)]
#
#    return "{frac:.0%} of data selected. {part:d} of {total:d} records selected.".format(
#        frac=fraction, part=part, total=total
#    ), {"color": color}


@callback(
    Output("plot-pa-distribution", "figure"),
    Input("select-pa-distribution", "value"),
)
def display_distribution(val):
    """
    displays distribution plot for selected column

        Input:
            selected_column: column selected in Dropdown
            distribution: chosen distribution
            slider_values: values of the sliders

        Returns:
            fig: figure of the distribution plot
    """

    fig = dbm.display_histogram(data_frame=pa, x=val)
    return fig


#######################################################################################
# PA QUALITY CALLBACKS
#######################################################################################
