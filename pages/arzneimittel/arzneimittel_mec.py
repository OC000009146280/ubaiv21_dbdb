import dash_bootstrap_components as dbc
import data_methods_load.datasets_auxiliary as dsa
import data_methods_load.methods as dbm
from dash import dash_table, dcc, html
from data_methods_load.datasets import arzneimittel_mec

arzneimittel_mec_display_list = dbm.get_config_list(dsa.config_mec, "display_histogram", "True")

##########################################################################################
# DASHBOARD DEFINITION
##########################################################################################

layout = dbc.Container(
    className="container-subpages-content-qual",
    #    style={"background-color": "#ffffff"},
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["content_all"]["explanation_time_geo"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            id="time-selection-mec-spinner",
                                            children=[
                                                dcc.Graph(
                                                    id="time-selection-arzneimittel-mec",
                                                    figure=dbm.get_time_barplot(
                                                        dsa.arzneimittel_mec_time_counts.index,
                                                        dsa.arzneimittel_mec_time_counts.values,
                                                    ),
                                                )
                                            ],
                                            color="primary",
                                        )
                                    ],
                                    width=6,
                                ),
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            id="map-mec-spinner",
                                            children=[
                                                dcc.Graph(
                                                    id="map_arzneimittel",
                                                    figure=dbm.display_Map(
                                                        data_frame=dbm.remap(
                                                            dsa.arzneimittel_mec_map_df,
                                                            "Sampling Country",
                                                            dsa.arzneimittel_mec_mapping["Sampling Country"],
                                                        ),
                                                        color=0,
                                                        locations="Sampling Country",
                                                        locationmode="country names",
                                                        scope="world",
                                                    ),
                                                )
                                            ],
                                            color="warning",
                                        )
                                    ],
                                    width=6,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["explanation_hist"]["text"],
                                            className="markdown-description",
                                        ),
                                        #                                        dcc.Markdown(
                                        #                                            dsa.markdown["content_all"]["explanation_hist_add_trace"]["text"],
                                        #                                            className="markdown-description",
                                        #                                        ),
                                    ],
                                    width=12,
                                )
                            ]
                        ),
                        html.Br(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Spinner(
                                            [
                                                dcc.Graph(
                                                    id="plot-distribution-mec",
                                                    figure=dbm.display_histogram(
                                                        data_frame=arzneimittel_mec[arzneimittel_mec_display_list],
                                                        x=arzneimittel_mec_display_list[0],
                                                    ),
                                                )
                                            ],
                                            color="warning",
                                        )
                                    ]
                                ),
                                dbc.Col(
                                    [
                                        dcc.Dropdown(
                                            id="select-column-distribution-mec",
                                            options=arzneimittel_mec_display_list,
                                            value=arzneimittel_mec_display_list[0],
                                        )
                                    ]
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["begin_halfsentece_restriction"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=4,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            id="selected-data-percentage-arzneimittel-mec",
                                        ),
                                    ],
                                    width=1,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["halfsentence_restriction_end"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=7,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["content_all"]["explanation_table"]["text"],
                            className="markdown-description",
                        ),
                        dash_table.DataTable(
                            id="mec-data-table",
                            columns=[{"name": col, "id": col} for col in arzneimittel_mec.columns],
                            data=arzneimittel_mec.sample(10).to_dict("records"),
                            fixed_rows={"headers": True},
                            style_table={"height": 550, "overflowX": "scroll"},
                            style_header={"backgroundColor": "#305D91", "padding": "10px", "color": "#FFFFFF"},
                            style_cell={
                                "textAlign": "center",
                                "minWidth": 95,
                                "maxWidth": 120,
                                "width": 100,
                                "font_size": "12px",
                                "whiteSpace": "normal",
                                "height": "auto",
                            },
                            filter_action="native",
                            sort_action="native",
                            sort_mode="multi",
                        ),
                    ]
                ),
            ]
        )
    ],
)
