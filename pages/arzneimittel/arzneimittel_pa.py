from dash import html, dcc, dash_table
from data_methods_load.datasets import arzneimittel_pa as pa
import data_methods_load.datasets_auxiliary as dsa
import dash_bootstrap_components as dbc
import data_methods_load.methods as dbm

##########################################################################################
# CALLBACKS
##########################################################################################

# @callback(
#     Output('pharma-agents-data-table', 'data'),
#     Input('dimension-selection', 'value'),
#     Input('dimension-dropdown', 'value')
# )
# def data_table_content(dimension, selection):
#     if dimension and selection:
#         selected_data = arzneimittel_pa.loc[arzneimittel_pa[dimension].isin(selection)]
#     else:
#         selected_data = arzneimittel_pa
#     return selected_data.to_dict('records')

# @callback(
#     Output('dimension-dropdown', 'options'),
#     Input('dimension-selection', 'value')
# )
# def dimension_dropdown_options(dimension):
#     if dimension:
#         return arzneimittel_pa.value_counts(subset=dimension).index.values
#     else:
#         return []

# @callback(
#     Output('selected-data-percentage-arzneimittel-pa', 'children'),
#     Output('selected-data-percentage-arzneimittel-pa', 'style'),
#     Input('dimension-selection', 'value'),
#     Input('dimension-dropdown', 'value')
# )
# def selected_data_percentage(dimension, selection):
#     total = len(arzneimittel_pa.index)
#     if dimension and selection:
#         part = len(arzneimittel_pa.loc[arzneimittel_pa[dimension].isin(selection)].index)
#     else:
#         part = total
#     fraction = part/total
#     color_steps = 10
#     color = colors.sample_colorscale(
#         'Bluered',
#         color_steps
#         )[-int(min(fraction,0.99)*color_steps)]

#     return '{frac:.0%} of data selected. {part:d} of {total:d} records selected.'.format(frac=fraction, part=part, total=total), \
#          {'color':color}


# @callback(
#     Output('plot-pa-distribution', 'figure'),
#     Input('select-pa-distro', 'value'),
# )
# def display_distro(val):
#     fig = pdm.display_time_histogram(
#                         df = arzneimittel_pa,
#                         df_col = val
#                     )
#     return fig
##########################################################################################
# DASHBOARD DEFINITION
##########################################################################################

# dash.register_page(__name__, name='Arzneimittel in der Umwelt - Substanzen')

layout = dbc.Container(
    className="container-subpages-content-qual",
    #    style={"background-color": "#ffffff"},
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["content_all"]["explanation_hist_overall"]["text"],
                    className="markdown-description",
                ),
                html.Hr(),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["explanation_hist"]["text"],
                                            className="markdown-description",
                                        ),
                                        #                                        dcc.Markdown(
                                        #                                            dsa.markdown["content_all"]["explanation_hist_add_trace"]["text"],
                                        #                                            className="markdown-description",
                                        #                                        ),
                                    ],
                                    width=12,
                                )
                            ]
                        ),
                        html.Br(),
                        dbc.Row(
                            [
                                dbc.Col([dcc.Graph(id="plot-pa-distribution")]),
                                dbc.Col(
                                    [
                                        dcc.Dropdown(
                                            id="select-pa-distribution",
                                            options=dbm.get_config_list(dsa.config_pa, "display_histogram", "True"),
                                            value=dbm.get_config_list(dsa.config_pa, "display_histogram", "True")[0],
                                        )
                                    ]
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["begin_halfsentece_restriction"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=4,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            id="selected-data-percentage-arzneimittel-pa",
                                        ),
                                    ],
                                    width=1,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["halfsentence_restriction_end"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=7,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["content_all"]["explanation_table"]["text"],
                            className="markdown-description",
                        ),
                        dash_table.DataTable(
                            id="pharma-agents-data-table",
                            columns=[{"name": col, "id": col} for col in pa.columns],
                            data=pa.to_dict("records"),
                            fixed_rows={"headers": True},
                            style_table={"height": 550, "overflowX": "scroll"},
                            style_header={"backgroundColor": "#305D91", "padding": "10px", "color": "#FFFFFF"},
                            style_cell={
                                "textAlign": "center",
                                "minWidth": 95,
                                "maxWidth": 120,
                                "width": 100,
                                "font_size": "12px",
                                "whiteSpace": "normal",
                                "height": "auto",
                            },
                            filter_action="native",
                            sort_action="native",
                            sort_mode="multi",
                        ),
                    ]
                ),
            ]
        )
    ],
)
