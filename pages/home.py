import dash
from dash import html
from dash import dcc
import dash_bootstrap_components as dbc


dash.register_page(__name__, path="/")


layout = dbc.Container(
    [
        html.Div(
            children=[
                html.Br(),
                html.Img(
                    src=dash.get_asset_url("UBA_Logo.png"),
                    height=60,
                    width=120,
                    style={"float": "right"},
                ),
                html.Br(),
                html.H2(
                    "DataBias Dashboard des Umweltbundesamtes",
                    className="Header",
                ),
                html.Br(),
                dcc.Markdown(
                    "Hier erhalten sie aktuelle Daten zur Umweltsituation in Deutschland."
                    " Wählen Sie dazu bitte einen der weiter unten aufgeführten Datensätze, um einen tieferen Einblick zu erhalten.",
                    className="markdown-description",
                ),
                html.Br(),
                html.Div(
                    children=[
                        html.Br(),
                        dbc.Row(
                            style={
                                "display": "flex",
                                "justify-content": "space-between",
                                "align-items": "stretch",
                            },
                            children=[
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardImg(
                                                    src=dash.get_asset_url("unfall.png"),
                                                    top=True,
                                                    id="CardImg_accidents",
                                                ),
                                                html.H4("Accidents", className="card-title"),
                                                html.P(
                                                    "Ein US-weiter Autounfalldatensatz, der 49 Bundesstaaten der USA abdeckt. "
                                                    "Die Unfalldaten werden von Februar 2016 bis Dezember 2021 erhoben. ",
                                                    className="card-text",
                                                ),
                                                dbc.Button(
                                                    "Overview",
                                                    color="primary",
                                                    outline=True,
                                                    href="/accidents/accidents-nav",
                                                    id="button_home_accidents",
                                                    style={"bottom": "0px", "position": "absolute", "width": "17rem"},
                                                ),
                                            ],
                                            style={"width": "18rem", "height": "24rem", "padding": "5px"},
                                            className="small-card",
                                            id="accidents_home_card",
                                        ),
                                    ],
                                    width={"size": 3},
                                ),
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardImg(
                                                    src=dash.get_asset_url("arzneimittel.png"),
                                                    top=True,
                                                    id="CardImg_arzneimittel",
                                                ),
                                                html.H4("Arzneimittel in dem Umwelt", className="card-title"),
                                                html.P(
                                                    " Ein Datensatz zu verschiedenen Arzneimitteln die bei Messungen "
                                                    " in der Umwelt festgestellt wurden. ",
                                                    className="card-text",
                                                ),
                                                dbc.Button(
                                                    "Overview",
                                                    color="primary",
                                                    outline=True,
                                                    href="/arzneimittel/arzneimittel-nav",
                                                    id="button_home_arzneimittel",
                                                    style={"bottom": "0px", "position": "absolute", "width": "17rem"},
                                                ),
                                            ],
                                            style={"width": "18rem", "height": "24rem", "padding": "5px"},
                                            className="small-card",
                                            id="arzneimittel_home_card",
                                        )
                                    ],
                                    width={"size": 3},
                                ),
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardImg(
                                                    src=dash.get_asset_url("pexels-cheminfo.png"),
                                                    top=True,
                                                    id="CardImg_cheminfo",
                                                ),
                                                html.H4("ChemInfo - Auszug", className="card-title"),
                                                html.P(
                                                    " Ein Datensatz zu verschiedenen Chemikalien "
                                                    " die von Herstellern eingereicht wurden. ",
                                                    className="card-text",
                                                ),
                                                dbc.Button(
                                                    "Overview",
                                                    color="primary",
                                                    outline=True,
                                                    href="/cheminfo/cheminfo-nav",
                                                    id="button_home_cheminfo",
                                                    style={"bottom": "0px", "position": "absolute", "width": "17rem"},
                                                ),
                                            ],
                                            style={"width": "18rem", "height": "24rem", "padding": "5px"},
                                            className="small-card",
                                            id="cheminfo_home_card",
                                        )
                                    ],
                                    width={"size": 3},
                                ),
                            ],
                        ),
                        html.Br(),
                    ],
                ),
                html.Br(),
                html.Br(),
            ],
        )
    ]
)
