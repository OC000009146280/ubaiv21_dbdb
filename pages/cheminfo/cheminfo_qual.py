from dash import html, dcc
import data_methods_load.datasets_auxiliary as dsa
from data_methods_load.methods import display_missing_barplot
import dash_daq as daq
import dash_bootstrap_components as dbc

#############################################################################################################
# LAYOUT
#############################################################################################################
layout = dbc.Container(
    className="container-subpages-content-qual",
    #    style={"background-color": "#ffffff"},
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["qual_all"]["explanation_columnbars"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dcc.Graph(id="msn-plot-chei", figure=display_missing_barplot(dsa.cheminfo_msn)),
                            ]
                        ),
                        # dbc.Row([
                        #     dbc.Col([dcc.Graph(id = "corr-plot-chei"),], width=6),
                        #     dbc.Col([daq.BooleanSwitch(id= "p-switch-chei", on=False, label="p-values", labelPosition="top"),], width=2),
                        # ])
                    ]
                ),
            ]
        )
    ],
)
