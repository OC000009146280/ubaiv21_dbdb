import dash
from dash import callback, html, dcc, Input, Output, State
import dash_bootstrap_components as dbc
import plotly.express as px
import data_methods_load.methods as dbm
from data_methods_load.datasets import cheminfo as chei
import data_methods_load.datasets_auxiliary as dsa
from pages.cheminfo import cheminfo
from pages.cheminfo import cheminfo_qual


#######################################################################################
#######################################################################################

dash.register_page(__name__)
layout = dbc.Container(
    [
        html.Div(
            className="container-subpages-content-qual",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.Breadcrumb(
                                items=[
                                    {"label": "Home", "href": "/", "external_link": True},
                                    {"label": "Cheminfo", "active": True},
                                ]
                            ),
                        ),
                        dbc.Col(
                            html.Img(
                                src=dash.get_asset_url("UBA_Logo.png"),
                                height=60,
                                width=120,
                                style={"float": "right"},
                            )
                        ),
                    ]
                ),
                html.H2(
                    children="Cheminfo",
                    className="Header",
                ),
                dbc.Row(
                    [
                        dbc.Col(
                            dcc.Graph(
                                id="chei-radio-graph",
                                figure=dbm.data_quality_radar(dsa.cheminfo_data_quality_scores),
                            ),
                        ),
                        dbc.Col(
                            html.Div(
                                [
                                    html.H4("Qualitätsmetriken", className="subheader"),
                                    dbc.Button(
                                        f"Informationsdichte: {round(dsa.cheminfo_data_quality_scores['Informationsdichte'], 3)}",
                                        color="light",
                                        id="chei-button-toggle-informationsdichte",
                                    ),
                                    dbc.Toast(
                                        header="Informationsdichte",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Informationsdichte"],
                                                    mathjax=True,
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="chei-infobox-informationsdichte",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Vollständigkeit: {round(dsa.cheminfo_data_quality_scores['Vollständigkeit'], 3)}",
                                        color="light",
                                        id="chei-button-toggle-vollständigkeit",
                                    ),
                                    dbc.Toast(
                                        header="Vollständigkeit",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Vollständigkeit"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="chei-infobox-vollständigkeit",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Singularität: {round(dsa.cheminfo_data_quality_scores['Singularität'], 3)}",
                                        color="light",
                                        id="chei-button-toggle-uniqueness",
                                    ),
                                    dbc.Toast(
                                        header="Singularität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Singularität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="chei-infobox-uniqueness",
                                        is_open=False,
                                    ),
                                    html.Br(),
                                    dbc.Button(
                                        f"Validität: {round(dsa.cheminfo_data_quality_scores['Validität'], 3)}",
                                        color="light",
                                        id="chei-button-toggle-Validität",
                                    ),
                                    dbc.Toast(
                                        header="Validität",
                                        children=[
                                            html.P(
                                                dcc.Markdown(
                                                    dsa.markdown["quality_indicators"]["Validität"], mathjax=True
                                                ),
                                                className="markdown-description",
                                            ),
                                        ],
                                        id="chei-infobox-Validität",
                                        is_open=False,
                                    ),
                                ]
                            )
                        ),
                    ]
                ),
                dcc.Markdown(
                    dsa.markdown["content_all"]["top-of-tabs"]["text"],
                    className="markdown-description",
                ),
                html.Br(),
                dcc.Tabs(
                    id="tabs-chei",
                    value="tab-chei-content",
                    children=[
                        dcc.Tab(
                            label="Inhalt",
                            value="tab-chei-content",
                            style={"background-color": "#ffffff"},
                            selected_style={"font-weight": "bold"},
                        ),
                        dcc.Tab(
                            label="Qualität",
                            value="tab-chei-quality",
                            style={"background-color": "#ffffff"},
                            selected_style={"font-weight": "bold"},
                        ),
                    ],
                ),
                html.Div(id="tabs-content-chei"),
                html.Br(),
                html.Footer(
                    [
                        html.P(
                            [
                                "Datensatz: ",
                                html.A(
                                    "ChemInfo (Ausschnitt)",
                                    href="https://www.umweltbundesamt.de/themen/chemikalien/informationssystem-chemikalien-d-bundes-d-laender#partnerlander-und-behorden",
                                    target="_blank",
                                ),
                            ],
                        ),
                        html.P(
                            [
                                "Bereitgestellt durch: ",
                                "Umweltbundesamt"
                                #                            html.A(
                                #                                "Kaggle", href="https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents", target="_blank"
                                #                            )
                            ],
                        ),
                    ]
                ),
            ],
        )
    ]
)

#######################################################################################
# NAVIGATOR CALLBACKS
#######################################################################################
@callback(Output("tabs-content-chei", "children"), [Input("tabs-chei", "value")])
def render_content(tab):
    if tab == "tab-chei-content":
        return cheminfo.layout
    elif tab == "tab-chei-quality":
        return cheminfo_qual.layout


# @callback(
#     Output("chei-radio-graph", "figure"),
#     Output("chei-button-toggle-informationsdichte", "children"),
#     Output("chei-button-toggle-vollständigkeit", "children"),
#     Output("chei-button-toggle-uniqueness", "children"),
#     Output("chei-button-toggle-Validität", "children"),
#     Input("time-selection-chei", "selectedData"),
# )
# def update_quality_radar(selected_time_data):
#     data = dbm._filter_time_selection(selected_time_data, data=chei, time_col="Year")
#     msn = dbm.data_conformity_checks(data, dsa.config_cheminfo)
#     corr = data.corr()
#     p = dbm.calculate_pvalues(data)
#     data_quality_scores = dbm.data_quality_scores(
#         correlations=corr,
#         p_values=p,
#         missing_values_df=msn,
#         dataset=data,
#     )
#     fig = dbm.data_quality_radar(data_quality_scores)
#     button1 = f"Informationsdichte: {round(data_quality_scores['Informationsdichte'], 3)}"
#     button2 = f"Vollständigkeit: {round(data_quality_scores['Vollständigkeit'], 3)}"
#     button4 = f"Singularität: {round(data_quality_scores['Singularität'], 3)}"
#     button5 = f"Validität: {round(data_quality_scores['Validität'], 3)}"
#     return fig, button1, button2, button4, button5


@callback(
    Output("chei-infobox-informationsdichte", "is_open"),
    Input("chei-button-toggle-informationsdichte", "n_clicks"),
    State("chei-infobox-informationsdichte", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("chei-infobox-vollständigkeit", "is_open"),
    Input("chei-button-toggle-vollständigkeit", "n_clicks"),
    State("chei-infobox-vollständigkeit", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("chei-infobox-uniqueness", "is_open"),
    Input("chei-button-toggle-uniqueness", "n_clicks"),
    State("chei-infobox-uniqueness", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


@callback(
    Output("chei-infobox-Validität", "is_open"),
    Input("chei-button-toggle-Validität", "n_clicks"),
    State("chei-infobox-Validität", "is_open"),
)
def toggle_collapse(n, is_open):
    """
    used to collapse/unfold one of the infoboxes about the quality radar
        Input:
            n: clicked on button 1/0
            is_open: current state of box
        Output:
            is_open: new state of box
    """
    if n:
        return not is_open
    return is_open


#######################################################################################
# CHEI CONTENT CALLBACKS
#######################################################################################


@callback(
    Output("chei-data-table", "data"),
    # Input("dimension-selection-chei", "value"),
    # Input("dimension-dropdown-chei", "value"),
    Input("time-selection-chei", "selectedData"),
)
def data_table_selection(
    # filter_dim,
    # filter_vals,
    selected_time_data,
):
    """
    used to filter the dataset based on geographical, temporal or value-related restrictions
        Input:
            filter_dim: column chosen for distribution plot
            filter_range: filters applied to distribution plot
            selected_time_data: filters applied to histogram
            selected_map_dat: filters applied to map
        Output:
            dictionary
    """
    # data = dbm._filter_distribution_dimension(filter_dim, filter_vals, data=chei)
    data = dbm._filter_time_selection(selected_time_data, data=chei, time_col="Year")
    # data = dbm._filter_country_select(selected_map_data, data, "Sampling Country")
    return data.sample(min(10, len(data))).to_dict("records")


@callback(
    Output("selected-data-percentage-chei", "children"),
    Output("selected-data-percentage-chei", "style"),
    # Input("dimension-selection-chei", "value"),
    # Input("dimension-dropdown-chei", "value"),
    Input("time-selection-chei", "selectedData"),
)
def selected_data_percentage(selected_time_data):
    """
    calculates percentage of selected data

        Input:
            filter_dim: column to filter
            filter_vals: filters applied to column
            selected_time_data: filters applied to histogram
            selected_map_dat: filters applied to map

        Returns:
            string: information about the percentage of data selected
    """
    # data = dbm._filter_dimension_dropdown(filter_dim, filter_vals, data=chei)
    data = dbm._filter_time_selection(selected_time_data, data=chei, time_col="Year")
    # data = dbm._filter_country_select(selected_map_data, data=data, location_mapping_col="Sampling Country")

    fraction = len(data.index) / len(chei.index)

    color_steps = 10
    color = px.colors.sample_colorscale("Rainbow", color_steps)[-int(min(fraction, 0.99) * color_steps)]
    return "{frac:.0%}".format(frac=fraction), {"color": color}


#######################################################################################
# CHEI QUALITY CALLBACKS
#######################################################################################
@callback(Output("corr-plot-chei", "figure"), Input("p-switch-chei", "on"))
def display_corrmap(switch):
    """
    displays either correlation heatmap or p-value-heatmap

        Input:
            switch: boolean

        Returns:
            fig: figure of the distribution plot
    """
    if switch:
        fig = dbm.display_corr_map(dsa.cheminfo_p, px.colors.sequential.Greys, "p")

    else:
        fig = dbm.display_corr_map(dsa.cheminfo_corr, px.colors.diverging.delta, "corr")
    return fig
