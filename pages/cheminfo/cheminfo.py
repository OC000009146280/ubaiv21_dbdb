from dash import html, dcc, dash_table
from data_methods_load.datasets import cheminfo
import data_methods_load.datasets_auxiliary as dsa
import dash_bootstrap_components as dbc
import data_methods_load.methods as dbm

#####################################################################
# LAYOUT
######################################################################

layout = dbc.Container(
    className="container-subpages-content-qual",
    children=[
        html.Div(
            children=[
                html.Br(),
                dcc.Markdown(
                    dsa.markdown["content_all"]["explanation_hist_overall"]["text"],
                    className="markdown-description",
                ),
                dcc.Markdown(
                    dsa.markdown["content_all"]["explanation_hist"]["text"],
                    className="markdown-description",
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Graph(
                                            id="time-selection-chei",
                                            figure=dbm.get_time_barplot(
                                                dsa.cheminfo_time_counts.index, dsa.cheminfo_time_counts.values
                                            ),
                                        )
                                    ],
                                    width=6,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["begin_halfsentece_restriction"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=4,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            id="selected-data-percentage-chei",
                                        ),
                                    ],
                                    width=1,
                                ),
                                dbc.Col(
                                    [
                                        dcc.Markdown(
                                            dsa.markdown["content_all"]["halfsentence_restriction_end"]["text"],
                                            className="markdown-description",
                                        ),
                                    ],
                                    width=7,
                                ),
                            ]
                        ),
                        html.Hr(),
                        dcc.Markdown(
                            dsa.markdown["content_all"]["explanation_table"]["text"],
                            className="markdown-description",
                        ),
                        dash_table.DataTable(
                            id="chei-data-table",
                            columns=[{"name": col, "id": col} for col in cheminfo.columns],
                            data=cheminfo.sample(10).to_dict("records"),
                            fixed_rows={"headers": True},
                            tooltip_header=dbm.get_config_property(dsa.config_cheminfo, "tooltip"),
                            style_table={"height": 550, "overflowX": "scroll"},
                            style_header={"backgroundColor": "#305D91", "padding": "10px", "color": "#FFFFFF"},
                            style_cell={
                                "textAlign": "center",
                                "minWidth": 95,
                                "maxWidth": 120,
                                "width": 100,
                                "font_size": "12px",
                                "whiteSpace": "normal",
                                "height": "auto",
                            },
                            filter_action="native",
                            sort_action="native",
                            sort_mode="multi",
                        ),
                    ]
                ),
            ]
        )
    ],
)
