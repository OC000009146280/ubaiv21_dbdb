from dash import Dash, html
import dash
import dash_bootstrap_components as dbc

external_stylesheets = [dbc.themes.BOOTSTRAP]

app = Dash(
    __name__,
    external_stylesheets=external_stylesheets,
    use_pages=True,
    suppress_callback_exceptions=True,
)
server = app.server

app.layout = html.Div(
    [
        dash.page_container
    ]
)

if __name__ == "__main__":
    app.run(
        debug=True,
    )
