# Einführung
Ziel dieses Projekts war es, ein Dashboard zu erstellen, welches einen strukturierten Überblick über die Qualität und den Inhalt verschiedener Datensätze geben kann. Als Grundlage des Dashboards wurden zunächst zwei Datensätze des Umweltbundesamtes (UBA) und ein externer Datensatz von Kaggle ausgewählt.

# Getting Started
1.	Installation:<br>
    - Um die Applikation ausführen zu können ist eine Installation von Python 3.9 mit verschiedenen Paketen 
    erforderlich. Diese sind in  ```requirements.txt``` aufgelistet. Durch das Ausführen des Befehls ```pip install -r requirements.txt``` in der Konsole werden die Pakete installiert.
    - Zur Einbindung von Kaggle-Datenbanken wie US-Accidents ist die Erstellung eines Kaggle-Accounts erforderlich, da Kaggle erst mit dem dann verfügbaren Token den Download der dort verfügbaren Daten erlaubt. Nach dem Download des Tokens auf Kaggle unter Account/API muss dieser unter ```C:\Users\<Nutzer>\.kaggle``` abgelegt werden 
    - Klonen oder Download des Codes der Applikation aus dem GitLab-Repository
    - ```app.py``` ausführen, um das Dashboard lokal zu starten
    - Ausführen von ```setup.py``` durch Nutzer mit ```python setup.py```, die Datenbanken werden automatisch heruntergeladen und verarbeitet`
    - Falls die Datenbanken nicht automatisch heruntergeladen werden können oder der Nutzer sie manuell hinzufügen möchte, sind die folgenden Schritte für das Setup auszuführen:
        - Manuelles herunterladen der Datenbanken Ablage im Unterordner ```C:\<Custom_App_Path\App>\data_methods_load\data\```
        - Die Funktion erkennt selbstständig, dass die Dateien bereits existieren, und überspringt den Download.
    - Ausführen von ```app.py```
        - Die Applikation benötigte unter Umständen bis zu einer Minute, um zu starten
    
2.	Erforderliche Packages:<br>
    - siehe ```requirements.txt```

# Build and Test
TODO: Describe and show how to build your code and run the tests. 

# Contribute
TODO: Explain how other users and developers can contribute to make your code better. 

If you want to learn more about creating good readme files then refer the following [guidelines](https://docs.microsoft.com/en-us/azure/devops/repos/git/create-a-readme?view=azure-devops). You can also seek inspiration from the below readme files:
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)