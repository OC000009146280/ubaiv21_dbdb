from typing import Literal
import plotly.express as px
import plotly.graph_objects as go
import dash_daq as daq
import time
from dash import html
import numpy as np
import scipy.stats as sps
import pandas as pd
import re
import os
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
from pathlib import Path
import requests
import warnings

warnings.filterwarnings("ignore")

##########################################################################################
# FILTER FUNCTIONS
##########################################################################################


def _filter_distribution_dimension(filter_dim: str, selected_data: dict, data: pd.DataFrame):
    """
    Filter data by categorical values.


        Args:
            filter_dim: dimension of the data, to which the categories belong
            filter_vals: values of categories to select
            data: pandas dataframe

        Returns:
            data with entries in the selected categories

    """
    if selected_data is None:
        return data
    if "range" in selected_data:
        try:
            return data.loc[data[filter_dim].between(*selected_data["range"]["x"])]
        except TypeError:
            values = [point["x"] for point in selected_data["points"]]
            return data.loc[data[filter_dim].isin(values)]
    else:
        values = [point["x"] for point in selected_data["points"]]
        return data.loc[data[filter_dim].isin(values)]


#    return data


def _filter_time_selection(selected_data: dict, data: pd.DataFrame, time_col: str):
    """
    Filter data by time

        Args:
            selected_data: dictionary containing the selected timeframe
            data: pandas dataframe
            time_col: column of data that contains information about time

        Returns:
            data with entries in the selected timeframe

    """
    if selected_data is None:
        return data
    if "range" in selected_data:
        return data.loc[data[time_col].between(*selected_data["range"]["x"])]
    else:
        values = [point["x"] for point in selected_data["points"]]
        return data.loc[data[time_col].isin(values)]


def _filter_country_select(selected_data: dict, data: pd.DataFrame, location_mapping_col: str):
    """
    Filter data by region

        Args:
            selected_data: dictionary containing the selected geografical region
            data: pandas dataframe
            location_mapping_col: column of data that contains information about the geographical region

        Returns:
            data with entries in the selected geographical region

    """

    if selected_data is not None:
        state = [x["location"] for x in selected_data["points"]]
        data = data.loc[data[location_mapping_col].isin(state)]

    return data


##########################################################################################
# HELPER FUNCTIONS
##########################################################################################
def str2float(data: pd.DataFrame, cols: list):
    """
    replaces commas in columns with dots and tries to convert them to float

        Input:
            data: dataframe
            cols: columns to be converted

        Output:
            df: dataframe
    """
    df = data.copy()
    for col in cols:
        df[col] = df[col].str.replace(",", ".")
        df[col] = df[col].astype(float)
    return df


def get_time_barplot(x: pd.Series, y: pd.Series):
    """
    draws barplot showing a time distribution of a DataFrame

        Args:
            x: takes the index of a value_counts()-Series
            y: takes the values of a value_counts()-Series

        Returns:
            Figure containing the barplot
    """
    tx = time.time()
    fig = px.bar(
        x=x,
        y=y,
        title="Zeitverteilung",
        labels={"x": "Jahr", "y": "Anzahl"},
    )
    fig.update_layout(clickmode="event+select")
    print(f"done executing get_time_barplot after {(time.time()-tx)*1e3:.2f} ms")
    return fig


def calculate_pvalues(df: pd.DataFrame):
    """
    calculates p_values of a given dataframe

        Args:
            df: pandas dataframe

        Returns:
            dataframe with p-values
    """
    df1 = df.dropna()._get_numeric_data()
    if len(df1) < 2:
        df1 = df.dropna(axis=1)
        df1 = df.dropna()._get_numeric_data()
    dfcols = pd.DataFrame(columns=df1.columns)
    pvalues = dfcols.transpose().join(dfcols, how="outer")
    for r in df1.columns:
        for c in df1.columns:
            try:
                pvalues[r][c] = round(sps.pearsonr(df1[r], df1[c])[1], 4)
            except ValueError:
                pvalues[r][c] = np.NAN
    return pvalues


def revert_selected_data_factoring(selectedData: dict, mapping_dict: dict):
    r"""
    Reverts factorization of map data in click+select selection

        Args:
            selectedData: dictionary containing the click+select selection
            mapping_dict: dictionary containing the mappings of the factorized dataframe

        Returns:
            selected data with the true names

    search location data {'points': [{'curveNumber': 0, 'pointNumber': 6, 'pointIndex': 6, 'location': 'CA', 'z': 253200}, {'curveNumber': 0, 'pointNumber': 6, 'pointIndex': 6, 'location': 'MI', 'z': 253200}]}

    """
    if selectedData is not None:

        rev = {v: k for k, v in mapping_dict["State"].items()}
        new_points = []
        for point in selectedData["points"]:
            point["location"] = int(rev[point["location"]])
            new_points.append(point)
        return {"points": new_points}

    return selectedData


def remap(df: pd.DataFrame, col_name: str, col_dict: dict):
    r"""
    Reverts factorization of map data

        Args:
            df: factorized data frame
            cname: column name of column to be refactorized
            col_dict : dictionary containing the mappings of the factorized column

        Returns:
            fd : data frame
    """
    fd = df.copy()

    col_dict = {eval(k.capitalize()): v for k, v in col_dict.items()}

    fd[col_name] = df[col_name].map(col_dict)
    return fd


def display_Map(**choropleth_kwargs):
    """
    creates a choropleth map

        Args:
            **choropleth_kwargs: arguments passed to px.choropleth

        Returns:
            fig: choropleth map

    """
    tx = time.time()
    fig_map = px.choropleth(**choropleth_kwargs)  # title="Räumliche Verteilung"
    fig_map.update_layout(
        autosize=True,
        # width=400,
        # height=300,
        margin=dict(l=0, r=0, t=0, b=0),
        clickmode="event+select",
        showlegend=False,
        coloraxis_showscale=False,
        title="Räumliche Verteilung",
    )
    print(f"done executing display_Map after {(time.time()-tx)*1e3:.2f} ms")
    return fig_map


def display_histogram(**hist_kwargs):
    """
    creates a histogram of the passed data_frame column

        Args:
            **hist_kwargs: keyword arguments passed to px.histogram

        Returns:
            fig: histogram
    """
    tx = time.time()
    fig_hist = px.histogram(**hist_kwargs)
    fig_hist.update_layout(
        autosize=True,
        # width=400,
        # height=300,
        showlegend=False,
        title="Histogramm der gewählten Spalte",
        xaxis_title="Wertebereich",
        yaxis_title="Anzahl",
        bargap=0.00,
        clickmode="event+select",
    )
    print(f"done executing display_histogram after {(time.time()-tx)*1e3:.2f} ms")
    return fig_hist


def display_missing_barplot(df: pd.DataFrame):
    """
    draws barplot with missing values per column, adapted from missingno

    Args:
        df: pandas dataframe

    Returns:
        fig: barplot

    """
    tx = time.time()
    fig = px.bar(
        data_frame=df,
        x=df.index,
        y=["notna", "conform"],
        title="Vorhandene Werte pro Spalte",
        barmode="overlay",
        labels={"variable": "Legende"},
    )
    label_map = {"notna": "Vorhandene Werte", "conform": "Valide Werte"}
    for trace in fig.data:
        trace.name = label_map.get(trace.name, trace.name)
    fig.update_layout(xaxis_title="Spalten", yaxis_title="Anzahl vorhandener Werte")
    # fig.update_traces(marker_color="gray")
    print(f"done executing display_missing_barplot after {(time.time()-tx)*1e3:.2f} ms")
    return fig


def display_corr_map(cor: pd.DataFrame, cmap: px.colors, mode: Literal["corr", "p"]):
    """
    draws correlation heatmap or p-values heatmap

        Args:
            corr: dataframe with correlations between columns, like data.corr()
            mask: ndarray with mask of correlation matrix like np.triu
            cmap: plotly colormap
            mode: either "corr" for correlation map or "p" for p-values map

        Returns:
            fig: figure

    """
    # triangular matrix map to remove redundant correlations
    mask = np.triu(np.ones_like(cor, dtype=bool))

    zmin = -1
    if mode == "p":
        zmin = 0
    tx = time.time()
    heat = go.Heatmap(
        z=cor.mask(mask),
        x=cor.columns,
        y=cor.columns,
        colorscale=cmap,
        reversescale=True,
        zmin=zmin,
        zmax=1,
    )

    fig = go.Figure(data=[heat])

    fig.update_xaxes(side="bottom")

    fig.update_layout(
        title_text="Korrelationen im Datensatz",
        title_x=0.5,
        width=1000,
        height=1000,
        xaxis_showgrid=False,
        yaxis_showgrid=False,
        xaxis_zeroline=False,
        yaxis_zeroline=False,
        yaxis_autorange="reversed",
        template="plotly_white",
    )

    for i in range(len(fig.layout.annotations)):
        if fig.layout.annotations[i].text == "nan":
            fig.layout.annotations[i].text = ""
    print(f"done executing display_corr_map after {(time.time()-tx)*1e3:.2f} ms")
    return fig


def get_ranges_dist(df_subs: pd.DataFrame, distribution: Literal["Normal", "Exponential"]):
    """
    gets distribution parameters for histogram plot

        Args:
            df_subs: data frame subsetted on column used in histogram plot
            distribution: distribution parameters to calculate, either Normal or Exponential

        Returns:
            tuple: tuple containing the parameters
    """
    if distribution == "Normal":
        return __get_ranges_normal_distribution(df_subs)
    if distribution == "Exponential":
        return __get_ranges_exponential_distribution(df_subs)


def __get_ranges_normal_distribution(df_subs):
    """
    gets slider ranges and distribution parameters for normal distribution like mean, standard deviation and Amplitude

        Args:
            df_subs: data frame subsetted on column used in histogram plot

        Returns:
            tuple: tuple containing the parameters and slider ranges
    """
    # boundaries for mean of normal distributionibution, depends on column
    mean_init = np.mean(df_subs)
    mean_min_boundary = df_subs.min()
    mean_max_boundary = df_subs.max()

    # boundaries for sd of normal distributionX, depends on column
    sd_init = np.std(df_subs)
    sd_min_boundary = 0
    sd_max_boundary = sd_init + sd_init * 5

    # boundaries and initial value for amplitude
    ampl_init = df_subs.between(mean_init - sd_init / 2, mean_init + sd_init / 2).sum() / (sd_init)
    ampl_min_boundary = 0
    ampl_max_boundary = ampl_init * 3

    # output
    init_parameters = [mean_init, sd_init, ampl_init]
    min_bounds_parameters = [mean_min_boundary, sd_min_boundary, ampl_min_boundary]
    max_bounds_parameters = [mean_max_boundary, sd_max_boundary, ampl_max_boundary]

    parameters = ["Mittelwert", "Standardabweichung", "Amplitude"]
    return init_parameters, min_bounds_parameters, max_bounds_parameters, parameters


def __get_ranges_exponential_distribution(df_subs):
    """
    gets slider ranges and distribution parameters for exponential distribution like Amplitude and Exponent

        Args:
            df_subs: data frame subsetted on column used in histogram plot

        Returns:
            tuple: tuple containing the parameters and slider ranges
    """
    # Number of values in the bottom 5% of the histogram determine initial amplitude
    n_bins = 100
    histogram_bin = (df_subs.max() - df_subs.min()) / n_bins
    print(f" _get_ranges_exponential_distribution histogram_bin {histogram_bin}")
    amplitude_initial = df_subs.loc[df_subs <= histogram_bin].size
    amplitude_min = 0
    amplitude_max = amplitude_initial * 1.1

    # Coarse exponential fit to estimate exponent
    # Form n bins, count members in each, take logarithm to linearise, estimate slope, transform
    exponent_initial = 1 / (
        np.mean(
            np.diff(
                np.log(
                    [max(1, df_subs.between(i * histogram_bin, (i + 1) * histogram_bin).sum()) for i in range(n_bins)]
                )
            )
        )
        / histogram_bin
    )
    # Set bounds accounting for negative exponent
    exponent_min = min(0, exponent_initial * 2)
    exponent_max = max(0, exponent_initial * 2)
    print(
        f" _get_ranges_exponential_distribution exponent_initial, exponent_min, exponent_max {exponent_initial, exponent_min, exponent_max}"
    )
    return (
        [amplitude_initial, exponent_initial],
        [amplitude_min, exponent_min],
        [amplitude_max, exponent_max],
        ["Amplitude", "Exponent"],
    )


def make_trace(df_subs: pd.DataFrame, distribution: Literal["Normal", "Exponential"], parameters: tuple[list]):
    """
    draws distribution trace into histogram

        Args:
            df_subs: data frame subsetted on column used in histogram plot
            distribution: distribution parameters to calculate, either Normal or Exponential
            parameters: distribution parameters

        Returns:
            fig: figure
    """
    trace = None
    if distribution == "Normal":
        trace = __make_normal_trace(df_subs, parameters)
    if distribution == "Exponential":
        trace = __make_exponential_trace(df_subs, parameters)
    return trace


def __make_normal_trace(df_subs, parameters):

    mean, standard_deviation, amplitude = parameters

    x = np.linspace(df_subs.min(), df_subs.max(), 100)

    # Calculating mean and standard deviation, mean of actual values

    # normal distribution
    y = sps.norm.pdf(x, mean, standard_deviation)

    # amplified distribution
    y_a = y / y.max() * amplitude

    # trace figure
    fig = go.Scatter(x=x, y=y_a, mode="lines", line=go.scatter.Line(), showlegend=False)

    return fig


def __make_exponential_trace(df_subs, parameters):
    amplitude, exponent = parameters
    x = np.linspace(df_subs.min(), df_subs.max(), 1000)
    y = sps.expon.pdf(x, exponent) * amplitude
    return go.Scatter(x=x, y=y, mode="lines", line=go.scatter.Line(), showlegend=False)


def data_quality_scores(correlations, p_values, missing_values_df, dataset):
    correlation_score = __correlation_score(correlations.values, p_values.values)
    completeness_score = __missing_value_score(missing_values_df["notna"])
    uniqueness_score = __uniqueness_score(dataset)
    validity_score = __validity_score(missing_values_df["conform"])
    return pd.Series(
        index=["Informationsdichte", "Vollständigkeit", "Singularität", "Validität"],
        data=[correlation_score, completeness_score, uniqueness_score, validity_score],
    )


def __uniqueness_score(dataset: pd.DataFrame):
    return (~dataset.duplicated()).sum() / len(dataset)


def __validity_score(valid_values):
    return valid_values.sum() / (len(valid_values) * valid_values.max())


def __correlation_score(correlations, p_values):
    # Correlation per dimension, weighted by significance. Crude approximation of entropy.
    # For more meaningful measure, implement multivariate entropy measure https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7515398/
    try:
        assert correlations.shape == p_values.shape
        n_features = correlations.shape[0]
        normalisation = n_features * (n_features - 1)
        return 1 - np.nansum(np.abs(correlations) * (1 - p_values)) / normalisation
    except ZeroDivisionError:
        return np.NAN


def __missing_value_score(missing_values):
    # Fraction of filled values, opposite of sparsity
    return missing_values.sum() / (len(missing_values) * missing_values.max())


def data_quality_radar(data_quality_scores: pd.DataFrame):
    dqs = data_quality_scores.dropna()
    return px.line_polar(
        r=dqs.values,
        theta=dqs.index,
        line_close=True,
        range_r=[0, 1],
        start_angle=45,
        width=450,
        height=400,
    )


def data_conformity_checks(dataset, config):
    """
    Take dataframe of full dataset and config file with rules to apply to each column.
    Return boolean dataframe in the same shape as input with true values where all applied rules match.
    """
    check_functions = {
        "numeric": __check_numeric,
        "range": __check_range,
        "natural": __check_natural,
        "valid_cas": __check_valid_cas,
        "regular_expression": __check_regular_expression,
    }
    conform_count = {}
    for label, content in dataset.items():
        # apply checks to column
        conform_values = content.notna()
        for check, args in config[label]["checks"].items():
            conform_values = conform_values & check_functions[check](content, args)
        conform_count[label] = conform_values.sum()
    return pd.DataFrame(data={"notna": dataset.notna().sum(), "conform": pd.Series(conform_count)})


def __check_numeric(column, args):
    if not eval(args):
        return column
    else:
        return ~pd.to_numeric(column, errors="coerce").isna()


def __check_range(column, args):
    if not len(args) == 2:
        return column
    else:
        return column.between(*args)


def __check_natural(column, args):
    if not eval(args):
        return column
    else:
        return pd.to_numeric(column) % 1 == 0


def __check_valid_cas(column, args):
    # returns CAS number if it is valid, returns 'Invalid' if CAS number does not follow the regular pattern for CAS numbers
    if not eval(args):
        return column
    pattern = "^\d{2,7}-\d{2}-\d$"

    def f(s):
        return bool(re.match(pattern, s)) and __cas_checksum(s)

    return column.apply(lambda s: f(s))


# internal function to check if the check number of the cas number is correct
def __cas_checksum(number):
    a, b, c = number.split("-")
    m = 1
    s = 0
    for d in (a + b)[::-1]:
        s += m * int(d)
        m += 1
    return int(c) == s % 10


def __check_regular_expression(column, args):
    if not eval(args):
        return column
    else:
        return column.apply(lambda s: bool(re.match(args, s)))


def get_config_list(df_config, feature, val):
    # gets a particular part of the features accoring to the configuration
    feature_dict = get_config_property(df_config, feature)
    return [key for key, value in feature_dict.items() if value == str(val)]


def get_config_property(df_config, property):
    return {label: properties[property] for label, properties in df_config.items()}


def __download(link: str, data_path):
    print("download started")
    response = requests.get(link)
    f_name = os.path.basename(response.url)
    print(f"downloading {f_name}")
    with open((data_path / f_name).resolve(), "wb") as f:
        f.write(response.content)

    try:
        assert os.path.exists((data_path / f_name).resolve())
    except AssertionError as e:
        print("The download does not seem to have generated the necessary files and placed them in the data directory")
        raise e
    return f_name


def __preprocess(preprocessing_file: str, data_path):
    print("Preprocessing dataset")
    with open((data_path / preprocessing_file).resolve(), "r") as f:
        nb = nbformat.read(f, as_version=4)

    ep = ExecutePreprocessor(timeout=600, kernel_name="python3")
    ep.preprocess(nb, {"metadata": {"path": data_path}})
    executed_file = "executed_" + preprocessing_file
    with open((data_path / executed_file).resolve(), "w", encoding="utf-8") as f:
        nbformat.write(nb, f)
    # try:
    #     assert os.path.exists((data_path / "ArzneimittelDB_compact_MEC.csv").resolve())
    #     assert os.path.exists((data_path / "Arzneimittel_MEC_maps.json").resolve())
    #     assert os.path.exists((data_path / "ArzneimittelDB_corrected_PA.csv").resolve())
    # except AssertionError as e:
    #     print("The preprocessing does not seem to have generated the necessary files and placed them in the data directory")
    #     raise e


def download_preprocess(data_path, file: str, prep_file: str = None):
    print(file)
    name = file.rsplit("/", 1)[-1]
    path = Path(data_path, name)
    if not os.path.exists(path):
        print("file not found - downloading...")
        f = __download(link=file, data_path=data_path)
    else:
        f = os.path.basename(file)
    if not None:
        __preprocess(preprocessing_file=prep_file, data_path=data_path)
    os.remove((data_path / f).resolve())


def find_numericals(config_dict):
    """Diese Funktion identifiziert numerische Spalten in einem config file um sie in Korrelationsmatrizen zu eliminieren"""
    l = []
    for elem in config_dict.keys():
        if config_dict[elem]["data_type"] == "num":
            l.append(elem)
    return l
