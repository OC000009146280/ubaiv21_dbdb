import pandas as pd
from pathlib import Path
import time

data_path = Path(__file__) / "../data"
markdown_path = Path(__file__) / "../../assets"

tx = time.time()

us_accidents = pd.read_csv(
    (data_path / "US_Accidents_Dec21_compact.csv").resolve(),
    sep=",",
    index_col=0,
    nrows=int(1e6),
    parse_dates=[2, 3, 15],
    engine="c",
)

us_accidents_correlations = pd.read_csv(
    (data_path / "US_Accidents_correlations.csv").resolve(), 
    sep=",", 
    index_col=[0, 1, 2], 
    engine="c"
)

us_accidents_correlations.name = "us_accidents_correlations"
arzneimittel_pa = pd.read_csv(
    (data_path / "ArzneimittelDB_corrected_PA.csv").resolve(), sep=",", index_col=0, engine="c"
)
arzneimittel_pa.Name = "arzneimittel_pa"

arzneimittel_mec = pd.read_csv(
    (data_path / "ArzneimittelDB_compact_MEC.csv").resolve(),
    sep=",",
    index_col=0,
    engine="c",  # to speed up loading
)
arzneimittel_mec.Name = "arzneimittel_mec"

cheminfo = pd.read_csv(
    (data_path / "cheminfo_lieferanten.csv").resolve(), 
    sep=",", 
    index_col=0, 
    engine="c"
).T.reset_index(drop=True)
cheminfo.Name = "cheminfo"

us_population = pd.read_csv((data_path / "us_population.csv").resolve(), sep=";", engine="c", usecols=[0, 1])

print(f"done loading all dataframes after {(time.time()-tx)*1e3:.2f} ms")
