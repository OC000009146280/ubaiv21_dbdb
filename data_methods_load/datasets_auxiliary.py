import data_methods_load.datasets as d
import data_methods_load.methods as dbm
import json
import time


ta = time.time()
"""Processing of US Accident Database"""
# add integer year column to dataframe from datetime type Start_Time
config_accidents = json.load(open((d.data_path / "config_accidents_new.json").resolve(), "r"))
d.us_accidents["Year"] = d.us_accidents.Start_Time.dt.year
# round values
d.us_accidents = round(d.us_accidents)
# counts by year for histogram
us_accidents_time_counts = d.us_accidents["Year"].value_counts().sort_index()
# counts by state for map
us_accidents_map_df = d.us_accidents.value_counts(subset="State").to_frame().reset_index()
# population by state added to map dataframe for per-capita calculation
us_accidents_map_df = us_accidents_map_df.join(d.us_population, on="State", how="left", rsuffix="_l")
# per-capita calculation using counts column from above and population numbers of 2021
us_accidents_map_df["per_capita"] = round(us_accidents_map_df[0] / us_accidents_map_df["pop_21"], 4)
# correlation matrix of all columns
us_accidents_corr = d.us_accidents_correlations.xs("correlation", level="quantity").unstack().droplevel(0, axis=1)
# p-values of correlation significance
us_accidents_p = d.us_accidents_correlations.xs("p_value", level="quantity").unstack().droplevel(0, axis=1)
numericals = dbm.find_numericals(config_accidents)
numericals.remove("Year")
us_accidents_corr = us_accidents_corr.loc[numericals, numericals]
us_accidents_p = us_accidents_p.loc[numericals, numericals]
# loading factorisation mapping and configuration files
us_accidents_geojson = json.load(open((d.data_path / "geojson_files" / "us-states_factor.json").resolve(), "r"))
us_accidents_mapping = json.load(open((d.data_path / "US_Accidents_maps.json").resolve(), "r"))
# pre-calculate data_quality scores
us_accidents_msn = dbm.data_conformity_checks(d.us_accidents, config_accidents)
us_accidents_data_quality_scores = dbm.data_quality_scores(  # initial calculation
    correlations=us_accidents_corr,
    p_values=us_accidents_p,
    missing_values_df=us_accidents_msn,
    dataset=d.us_accidents,
)
print(f"done processing us_accidents after {(time.time()-ta)*1e3:.2f} ms")

tb = time.time()
"""Processing of Arzneimittel in der Umwelt Measurements Database"""
# counts by sampling country for map
arzneimittel_mec_map_df = d.arzneimittel_mec.value_counts(subset="Sampling Country").to_frame().reset_index()
# counts by year for histogram, changing missing values from -9999 to 1900 for easier display
arzneimittel_mec_time_counts = (
    d.arzneimittel_mec["Sampling Period Start"].replace(-9999, 1900).value_counts().sort_index()
)

config_mec = json.load(open((d.data_path / "config_mec.json").resolve(), "r"))
# calculation of correlation matrix and p-values
arzneimittel_mec_corr = d.arzneimittel_mec.corr()
arzneimittel_mec_p = dbm.calculate_pvalues(d.arzneimittel_mec)
arzneimittel_mec_mapping = json.load(open((d.data_path / "Arzneimittel_MEC_maps.json").resolve(), "r"))
# counts of filled entries per column for data quality view
arzneimittel_mec_msn = dbm.data_conformity_checks(d.arzneimittel_mec, config_mec)

arzneimittel_mec_data_quality_scores = dbm.data_quality_scores(
    correlations=arzneimittel_mec_corr,
    p_values=arzneimittel_mec_p,
    missing_values_df=arzneimittel_mec_msn,
    dataset=d.arzneimittel_mec,
)
print(f"done processing arzneimittel_mec after {(time.time()-tb)*1e3:.2f} ms")

tc = time.time()
"""Processing of Arzneimittel in der Umwelt Pharma Agents Database"""
arzneimittel_pa_corr = d.arzneimittel_pa.corr()
arzneimittel_pa_p = dbm.calculate_pvalues(d.arzneimittel_pa)

config_pa = json.load(open((d.data_path / "config_PA.json").resolve(), "r"))
# Clear correöatopms
# counts of filled entries per column for data quality view
arzneimittel_pa_msn = dbm.data_conformity_checks(d.arzneimittel_pa, config_pa)

arzneimittel_pa_data_quality_scores = dbm.data_quality_scores(
    correlations=arzneimittel_pa_corr,
    p_values=arzneimittel_pa_p,
    missing_values_df=arzneimittel_pa_msn,
    dataset=d.arzneimittel_pa,
)
print(f"done processing arzneimittel_pa after {(time.time()-tc)*1e3:.2f} ms")

td = time.time()
"""Processing of Cheminfo Lieferanten Database"""
# add integer year column to dataframe transforming from ERSTDAT string
d.cheminfo["Year"] = d.cheminfo.ERSTDAT.str[:4].astype(int)
# counts by year for histogram
cheminfo_time_counts = d.cheminfo["Year"].value_counts().sort_index()

# calculation of correlation matrix and p-values
cheminfo_corr = d.cheminfo.corr()
cheminfo_p = dbm.calculate_pvalues(d.cheminfo)
# loading configuration file
config_cheminfo = json.load(open((d.data_path / "config_cheminfo.json").resolve(), "r"))
# counts of filled entries per column for data quality view
cheminfo_msn = dbm.data_conformity_checks(d.cheminfo, config_cheminfo)
cheminfo_data_quality_scores = dbm.data_quality_scores(
    correlations=cheminfo_corr,
    p_values=cheminfo_p,
    missing_values_df=cheminfo_msn,
    dataset=d.cheminfo,
)

# markdown = json.load(open(os.path.join(os.getcwd(), "assets", "markdown.json"), "r"))
markdown = json.load(open((d.markdown_path / "markdown.json").resolve(), "r", encoding="UTF-8"))
print(f"done processing cheminfo after {(time.time()-td)*1e3:.2f} ms")
